# GitHub Flow 介紹
## 什麼是 GitHub Flow ?
一言以敝之，GitHub Flow 是一個基於分支（Branch）的輕量化工作流程，幫助團隊及專案定期的進行部署。

看完了上述的說明還是不太懂嗎？沒關係我們先來看下面這張圖。

<div style="text-align: center">
    <img src="./src/github_flow_1.png" height="400px" />
</div>

簡單來說，我們會有一個最主要的分支 (master)，當我們要開發新的功能或是修改 Bug 時，我們不會直接修改 master 分支，而是會先從 master 切出一個 branch，當我們在這個 branch 修改並測試完成後，會發出 Pull Request (在 GitLab 稱作 Merge Request)，最後由 Project Owner 測試沒問題後將分支 Merge 回 master 就大功告成了，此時也可以順手將已 Merge 過的 branch 刪除。

### **重點規範！！！**
1. 要確保任何在 master 分支中的程式都是可部署的
1. 當在進行一個功能開發或修復時會分離出新分支，分支命名必須具有描述性，讓其他人清楚知道分支正在進行的工作項目
1. 每一個 commit 必須要寫 Commit Message，讓其他人清楚這個 Commit 究竟修改了什麼，這邊不要求 Commit Message 一定得用英文撰寫 (可用中文)，這邊提供 Commit Message 的簡單介紹，大家有空可以看一下 ([如何寫一個 Git Commit Message])。

---
## 參考資料
1. [讓我們來了解 GitHub Flow 吧！]
1. [GitHub Flow 及 Git Flow 流程使用時機]

[讓我們來了解 GitHub Flow 吧！]: https://medium.com/@trylovetom/%E8%AE%93%E6%88%91%E5%80%91%E4%BE%86%E4%BA%86%E8%A7%A3-github-flow-%E5%90%A7-4144caf1f1bf "讓我們來了解 GitHub Flow 吧！"
[GitHub Flow 及 Git Flow 流程使用時機]: https://blog.wu-boy.com/2017/12/github-flow-vs-git-flow/comment-page-1/ "GitHub Flow 及 Git Flow 流程使用時機"
[如何寫一個 Git Commit Message]: https://blog.louie.lu/2017/03/21/%E5%A6%82%E4%BD%95%E5%AF%AB%E4%B8%80%E5%80%8B-git-commit-message/ "如何寫一個 Git Commit Message"