# GitLab 基本操作
## 前言
在上一篇文章中我們針對 GitHub 做了簡單的介紹，本篇我們會講解如何在 GitLab 中執行 GitHub Flow，在開始以前我們先回憶一下完整的 GitHub Flow 我們究竟需要做哪些事情呢，請看下方流程。

> clone 專案 > 建立 branch > 在你的電腦切換至該 branch > 寫程式或除錯並進行測試 > commit 程式碼 > 將程式碼推送至 GitLab (push) > 發送 Merge Request 請求 > Project Owner 審核 > Merge 分支 > 完成

看起來步驟好像很多很麻煩，但其實操作起來並沒有想像中困難喔，接下來我們會對每個步驟做詳細的說明，就讓我們繼續看下去吧！

## 1. clone 專案
首先我們進入到 GitLab 的專案頁面，在網頁中我們可以看到 clone 這個按鈕，按下去後就可以看到 Clone with HTTPS 的連結網址，此時我們透過 git clone 指令或是透過 git gui 工具就可以將這個專案 clone 一份到自己的電腦裡囉。

<div style="text-align: center">
    <img src="./src/GitLab_MR/GitLab_MR_1_1.png" height="400px" />
</div>

## 2. 建立分支 (branch)
在我們要開發新的功能或是修改 bug 時，通常不會直接對 master 這個分支做修改，而是從 master 建立一個新的分支，在這個分支修改完成後才合併 (merge) 回 merge 這個分支，這樣做的好處是能確保 merge 裡的程式永遠都是處於功能無誤可以隨時上線的版本。

<div style="text-align: center">
    <img src="./src/GitLab_MR/GitLab_MR_2_1.png" height="400px" />
    在 GitLab 建立分支
</div>

<div style="text-align: center">
    <img src="./src/GitLab_MR/GitLab_MR_2_2.png" height="400px" />
    設定該分支的名稱及來源
</div>

<div style="text-align: center">
    <img src="./src/GitLab_MR/GitLab_MR_2_3.png" height="400px" />
    建立分支完成
</div>

## 3. 在你的電腦切換至該 branch
前置作業都完成的差不多了，是時候要來開始寫程式了吧！但在寫程式之前，記得先將自己電腦中的分支從 master 切換到我們要修改的分支喔，在這邊我是使用 GitKeaen 這個 git gui 程式，也可以依照自己的喜好選擇其他工具，例如 SourceTree 或是最單純的 git 指令都是可以的喔。
<div style="text-align: center">
    <img src="./src/GitLab_MR/GitLab_MR_3_1.png" height="400px" />
    GitKeaen 操作介面
</div>

## 4. 寫程式或除錯並進行測試
這個步驟就請大家貢獻自己的~~心臟~~才能，盡情的為專案寫出更多好用的功能吧～ 

## 5. commit 程式碼
當我們寫好熱騰騰的功能或是告一個小段落時，千萬別忘記要養成常常 commit 的好習慣，以免好不容易寫的的功能不小心被自己改壞甚至不小心刪除，那就欲哭無淚了啊，commit 時必須用淺顯易懂的話將本次修改的內容做註解，這樣大家就很容易的可以知道你做了哪些修改喔。

<div style="text-align: center">
    <img src="./src/GitLab_MR/GitLab_MR_5_1.png" height="400px" />
    GitKeaen commit 介面
</div>

## 6. 將程式碼推送至 GitLab (push)
現在我們的功能已經開發完畢，也經過萬全的測試後，就可以將我們的修改推送至 GitLab，完成 push 動作後，就可以在 GitLab 中看到我們所修改的內容囉。

<div style="text-align: center">
    <img src="./src/GitLab_MR/GitLab_MR_6_1.png" height="400px" />
    GitKeaen push 介面
</div>

## 7. 發送 Merge Request 請求
最後我們當然不希望用心做出來的功能沒人用，所以我們需要跟專案的負責人說我們做了這個好用的功能，請快點讓他上線吧！這時候就得發送 Merge Request 請求，告訴專案負責人做了哪些修改，請專案負責人檢查程式有沒有問題，若是沒有問題的話新功能上線的日子就指日可待啦～
<div style="text-align: center">
    <img src="./src/GitLab_MR/GitLab_MR_7_1.png" height="400px" />
    在自己的分支中送出 Merge Request
</div>

<div style="text-align: center">
    <img src="./src/GitLab_MR/GitLab_MR_7_2.png" height="400px" />
    說明這個分支中你做了什麼事，或是有什麼話想跟專案負責人說的都可以打在這裡
</div>

<div style="text-align: center">
    <img src="./src/GitLab_MR/GitLab_MR_7_3.png" height="400px" />
    送出 Merge Request
</div>

## 總結
看到這裡，對於 GitLab 的操作應該都有一定程度上的瞭解了吧，剩下的就是多多練習，熟悉操作流程，以後當別人問你 Git 的操作你怎麼都可以用得這麼順手時，你就可以很跩得的他說「我亦無他，惟手熟爾」。 XD

但若你看完以上說明後，對於 Git 的專有名詞和運作原理還是不太懂的話，沒關係這邊附上一些教學網站供大家學習，也換迎大家使用 test_project 這個專案進行練習，有問題也歡迎隨時提出來討論，希望大家都能夠一起成長，謝謝收看，咱們下次見囉。

## 學習資源
1. [猴子都能懂的Git入門](https://backlog.com/git-tutorial/cn/)
1. [GIT教學](https://kingofamani.gitbooks.io/git-teach/content/chapter_2/git.html)
